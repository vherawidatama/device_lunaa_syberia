#
# Copyright (C) 2023 The syberiaOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/syberia_lunaa.mk

COMMON_LUNCH_CHOICES := \
    syberia_lunaa-user \
    syberia_lunaa-userdebug \
    syberia_lunaa-eng
